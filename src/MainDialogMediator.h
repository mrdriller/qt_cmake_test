#pragma once

class MainDialogInterface
{
public:
    virtual ~MainDialogInterface() {}
    virtual void EnableGoButton(bool enabled) = 0;
};

class MainDialogMediator
{
public:
    MainDialogMediator(MainDialogInterface *dialog);
    ~MainDialogMediator();
};

