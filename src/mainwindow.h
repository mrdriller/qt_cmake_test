#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "MainDialogMediator.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow, public MainDialogInterface
{
    Q_OBJECT

public slots:
    void on_goButton_clicked();
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    virtual void EnableGoButton(bool enabled);
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
