#include <iostream>
#include <QApplication>
#include "mainwindow.h"
#include "MainDialogMediator.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    MainDialogMediator mediator(&w);
    w.show();

    return a.exec();
}