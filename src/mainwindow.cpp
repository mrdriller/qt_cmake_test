#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_goButton_clicked()
{
    std::cout << "Inside MainWindow::on_goButton_clicked()" << std::endl;
}

void MainWindow::EnableGoButton(bool enabled)
{
    ui->goButton->setEnabled(enabled);
}