#include "MainDialogTestDouble.h"
#include "MainDialogMediator.h"
#include <gtest/gtest.h>

TEST(MainDialogTest, GoButtonInitiallyDisabled) {
    MainDialogTestDouble dialog;
    MainDialogMediator mediator(&dialog);
    EXPECT_TRUE(dialog.EnableGoButtonCalled);
    EXPECT_FALSE(dialog.EnableGoButtonLastEnabled);
}