#include <gtest/gtest.h>

//TEST(SimpleSanityTest, Booleans) {
//    EXPECT_EQ(false, !true);
//    EXPECT_EQ(true, !false);
//}
//
//TEST(SimpleSanityTest, IntegerArithmetic) {
//    EXPECT_EQ(17, 13 + 4);
//    EXPECT_EQ(9, 13 - 4);
//    EXPECT_EQ(52, 13 * 4);
//    EXPECT_EQ(3, 13 / 4);
//}
//
//TEST(SimpleSanityTest, FloatingPointArithmetic)
//{
//    EXPECT_EQ(17.0, 13.0 + 4.0);
//    EXPECT_EQ(9.0, 13.0 - 4.0);
//    EXPECT_EQ(52.0, 13.0 * 4.0);
//    EXPECT_EQ(3.25, 13.0 / 4.0);
//}

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}