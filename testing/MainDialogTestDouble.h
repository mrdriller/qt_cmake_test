#pragma once

#include "MainDialogMediator.h"

class MainDialogTestDouble : public MainDialogInterface
{
public:
    MainDialogTestDouble() : EnableGoButtonCalled(false), EnableGoButtonLastEnabled(false) {}
    virtual ~MainDialogTestDouble() {}
    bool EnableGoButtonCalled;
    bool EnableGoButtonLastEnabled;

    virtual void EnableGoButton(bool enabled);
};