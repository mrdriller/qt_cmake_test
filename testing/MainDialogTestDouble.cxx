#include "MainDialogTestDouble.h"

void MainDialogTestDouble::EnableGoButton(bool enabled)
{
    EnableGoButtonCalled = true;
    EnableGoButtonLastEnabled = enabled;
}
